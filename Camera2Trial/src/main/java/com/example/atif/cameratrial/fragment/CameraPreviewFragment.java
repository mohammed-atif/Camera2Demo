package com.example.atif.cameratrial.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.camera2.CameraCharacteristics;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.atif.cameratrial.list_items.PreviewListItem;
import com.example.atif.cameratrial.R;
import com.example.atif.cameratrial.adapters.PreviewChooserAdapter;
import com.example.atif.cameratrial.camera.CustomCamera;
import com.example.atif.cameratrial.constants.Constants;
import com.example.atif.cameratrial.custom_views.CustomCameraPreview;
import com.example.atif.cameratrial.exceptions.CameraException;
import com.example.atif.cameratrial.interfaces.CustomCameraInterface;
import com.example.atif.cameratrial.interfaces.UpdateDisplaySize;
import com.example.atif.cameratrial.utils.FragmentWithLogs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CameraPreviewFragment extends FragmentWithLogs implements CustomCameraInterface, View.OnClickListener, View.OnTouchListener, GestureDetector.OnGestureListener, UpdateDisplaySize {

    private Context mContext;
    private CustomCamera mCamera;
    private CustomCameraPreview mCustomCameraPreview;
    private FloatingActionButton cameraButton, switchCamera, setupButton, cameraExposure;
    private ImageView preview;
    private View setupLayout;
    private int currentCameraFacing = CameraCharacteristics.LENS_FACING_BACK;
    private RelativeLayout seekBarLayout;
    private SeekBar seekBar;
    private TextView seekStatus, done;
    private List<Size> portraitOutputs;
    private List<Size> landscapeOutputs;
    private List<PreviewListItem> totalOutputs;
    private GestureDetector gestureDetector;
    private ImageView focusImageView;
    private int orientation = Configuration.ORIENTATION_PORTRAIT;
    private RecyclerView recyclerView;
    private PreviewChooserAdapter adapter;
    private Range<Long> exposureTime;
    private Range<Integer> exposureLevel;

    //Saved Instance Keys
    private final String CURRENT_CAMERA_FACING = "CurrentCameraFacing";

    private final int CAMERA_PERMISSION = 11000;
    private final int STORAGE_PERMISSION = 11001;
    private final String TAG = this.getClass().getSimpleName();

    public CameraPreviewFragment() {
        super.TAG = TAG;
        // Required empty public constructor
    }

    public static CameraPreviewFragment newInstance() {
        return new CameraPreviewFragment();
    }

    //region Lifecycle Methods

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orientation = mContext.getResources().getConfiguration().orientation;
        gestureDetector = new GestureDetector(mContext, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_camera_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCustomCameraPreview = (CustomCameraPreview) view.findViewById(R.id.cameraPreview);
        cameraButton = (FloatingActionButton) view.findViewById(R.id.cameraButton);
        switchCamera = (FloatingActionButton) view.findViewById(R.id.switchCamera);
        setupButton = (FloatingActionButton) view.findViewById(R.id.setupOptions);
        cameraExposure = (FloatingActionButton) view.findViewById(R.id.changeExposure);
        preview = (ImageView) view.findViewById(R.id.imagePreview);
        seekBarLayout = (RelativeLayout) view.findViewById(R.id.seekLayout);
        seekBar = (SeekBar) seekBarLayout.findViewById(R.id.seekBar);
        seekStatus = (TextView) seekBarLayout.findViewById(R.id.seekStatus);
        done = (TextView) seekBarLayout.findViewById(R.id.done);
        focusImageView = (ImageView) view.findViewById(R.id.focus_image_view);
        recyclerView = (RecyclerView) view.findViewById(R.id.preview_chooser);
        setupLayout = view.findViewById(R.id.camera_switches);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(CURRENT_CAMERA_FACING)){
            currentCameraFacing = savedInstanceState.getInt(CURRENT_CAMERA_FACING);
            Log.d(TAG, "current camera facing "+currentCameraFacing);
        }
        portraitOutputs = new ArrayList<>();
        landscapeOutputs = new ArrayList<>();
        totalOutputs = new ArrayList<>();
        adapter = new PreviewChooserAdapter(mContext, totalOutputs, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        requestCameraPermission();
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG, "current progress is "+progress);
                long minTime = exposureTime.getLower();
                long maxTime = exposureTime.getUpper();
                long currentTime = (maxTime - minTime)*progress/100 + minTime;
                seekStatus.setText(String.valueOf(currentTime));
                mCamera.setExposureTime(currentTime);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        cameraButton.setOnClickListener(this);
        switchCamera.setOnClickListener(this);
        mCustomCameraPreview.setOnTouchListener(this);
        focusImageView.setOnClickListener(this);
        cameraExposure.setOnClickListener(this);
        setupButton.setOnClickListener(this);
        done.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeCamera();
        exposureLevel = mCamera.getExposureLevels();
        exposureTime = mCamera.getExposureDuration();
        Log.d(TAG, "Exposure time : "+exposureTime);
    }

    @Override
    public void onPause() {
        mCamera.pauseCamera();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_CAMERA_FACING, currentCameraFacing);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CAMERA_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            initializeCamera();
        }
        if(requestCode == STORAGE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            takePicture();
        }
    }

    //endregion

    //region Private Methods

    private void initializeCamera(){
        if(mCamera == null){
            mCamera = new CustomCamera(mContext, this, currentCameraFacing);
            mCamera.setCameraPreview(mCustomCameraPreview);
            switchCamera.setSelected(currentCameraFacing == CameraCharacteristics.LENS_FACING_FRONT);
        }
        if(isResumed()){
            try {
                mCamera.resumeCamera();
            } catch (CameraException e) {
                e.printStackTrace();
            }
        }
        getCameraDetails();
    }

    private void getCameraDetails(){
        Range<Integer> exposure = mCamera.getExposureLevels();
        if(exposure != null) {
            Log.i(TAG, "Exposure low : " + exposure.getLower() + " exposure max : " + exposure.getUpper());
        }else{
            Log.i(TAG, "No exposure details found");
        }
        Log.i(TAG, "Camera minimum focal length : "+mCamera.getCameraMinFocalLength());
        Log.i(TAG, "Camera max AF regions : "+mCamera.getMaxAfRegions());
        Log.i(TAG, "Camera max AE regions : "+mCamera.getMaxAeRegions());
        Log.i(TAG, "Camera max AWB regions : "+mCamera.getMaxAwbRegions());
        populatePreviewList();
    }

    private void populatePreviewList(){
        List<Size> cameraSupportSize = mCamera.getCameraSupportedSize();
        for(Size size: cameraSupportSize){
            getCalculatedMegaPixel(size);
        }
        adapter.notifyDataSetChanged();
    }

    private void takePicture(){
        if(ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            mCamera.takePicture();
            cameraButton.setSelected(true);
            setupLayout.setVisibility(View.GONE);
        }else{
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION);
        }
    }

    private float getCalculatedMegaPixel(Size currentSize){
        float width = currentSize.getWidth();
        float height = currentSize.getHeight();
        float ratio = width/height;
        long conversionFactor = 1000000;
        float tempwidth = width*width;
        if(width <= height){
            portraitOutputs.add(currentSize);
        }
        if(width >= height){
            landscapeOutputs.add(currentSize);
        }
        float megaPixels = tempwidth/(ratio*conversionFactor);
        Log.d(TAG, "Megapixels : "+megaPixels);
        totalOutputs.add(new PreviewListItem(currentSize, megaPixels));
        return megaPixels;
    }

    private void galleryAddPic(String mCurrentPhotoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        mContext.sendBroadcast(mediaScanIntent);
    }

    //endregion

    //region Camera and related Interface Methods

    @Override
    public void onImageProcessed(final String imagePath) {
        Log.d(TAG, imagePath );
        ((Activity)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                preview.setVisibility(View.VISIBLE);
                Glide.with(mContext)
                        .load(new File(imagePath))
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(preview);
            }
        });
        galleryAddPic(imagePath);
    }



    @Override
    public void requestCameraPermission() {
        Log.i(TAG, "requesting camera permission and starting the camera");
        if(ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            initializeCamera();
        }else{
            ActivityCompat.requestPermissions((Activity) mContext,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CAMERA_PERMISSION);
        }
    }

    @Override
    public void notifyErrorOccurred(String error) {
        Log.e(TAG, error);
    }

    @Override
    public void updateDisplaySize(Size currentSize) {
        recyclerView.setVisibility(View.GONE);
        mCamera.setCurrentImageCaptureSize(currentSize);
    }

    //endregion

    //region ClickListener

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.cameraButton:{
                if(cameraButton.isSelected()){
                    preview.setVisibility(View.GONE);
                    cameraButton.setSelected(false);
                    setupLayout.setVisibility(View.VISIBLE);
                }else {
                    takePicture();
                }
                break;
            }
            case R.id.setupOptions:{
                recyclerView.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.changeExposure:{
                if(cameraExposure.isSelected()){
                    cameraExposure.setSelected(false);
                    mCamera.cancelExposureSettings();
                }else {
                    cameraExposure.setSelected(true);
                    seekBarLayout.setVisibility(View.VISIBLE);
                }
                break;
            }
            case R.id.switchCamera:{
                try {
                    mCamera.switchCamera();
                    currentCameraFacing = mCamera.getCurrentCameraFacing();
                    switchCamera.setSelected(currentCameraFacing == CameraCharacteristics.LENS_FACING_FRONT);
                } catch (CameraException e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.focus_image_view:{
                mCamera.cancelFocusRegion();
                focusImageView.setVisibility(View.GONE);
                break;
            }
            case R.id.done:{
                seekBarLayout.setVisibility(View.GONE);
                break;
            }
        }

    }

    //endregion

    //region TouchListener Methods

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.d(TAG, "Omn touch called");
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        Log.d(TAG, "Focus tap called");
        int width = (int) (56* Constants.ScreenData.CONVERSION_FACTOR);
        int x = (int) event.getX();
        int y = (int) event.getY();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) focusImageView.getLayoutParams();
        params.setMargins(x, y, 0, 0);
        focusImageView.setLayoutParams(params);
        mCamera.setFocusRegion(x, y, width);
        focusImageView.setVisibility(View.VISIBLE);
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    //endregion
}
