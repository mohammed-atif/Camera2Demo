package com.example.atif.cameratrial.interfaces;

/**
 * Created by atif on 22/01/17.
 */

public interface CustomCameraInterface{
    /**
     * <h3>Provides the path to the image captured</h3>
     * This callback method is called once the image is processed
     * @param imagePath temporary path of the image after capturing the Image
     */
    void onImageProcessed(String imagePath);

    /**
     *Notifies the activity about missing Camera Permission
     */
    void requestCameraPermission();

    /**
     * Notifies the activity/fragment in case any error occurs
     * @param error Type or description of the error occured
     */
    void notifyErrorOccurred(String error);
}