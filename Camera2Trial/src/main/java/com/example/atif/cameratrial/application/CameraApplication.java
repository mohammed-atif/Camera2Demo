package com.example.atif.cameratrial.application;

import android.app.Application;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.example.atif.cameratrial.constants.Constants;

/**
 * Created by atif on 09/02/17.
 */

public class CameraApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        Constants.ScreenData.CONVERSION_FACTOR = (float) metrics.widthPixels/360.0f;
    }
}
