package com.example.atif.cameratrial.exceptions;

/**
 * Created by atif on 22/01/17.
 */

public class CameraException extends Exception {
    public CameraException(String message) {
        super(message);
    }
}

