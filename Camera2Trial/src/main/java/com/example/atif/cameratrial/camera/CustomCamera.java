package com.example.atif.cameratrial.camera;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.Surface;

import com.example.atif.cameratrial.custom_views.CustomCameraPreview;
import com.example.atif.cameratrial.exceptions.CameraException;
import com.example.atif.cameratrial.interfaces.CustomCameraInterface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static android.hardware.camera2.CameraDevice.StateCallback;
import static android.media.ImageReader.OnImageAvailableListener;
import static android.view.TextureView.SurfaceTextureListener;
import static com.example.atif.cameratrial.camera.CustomCamera.CameraState.STATE_PICTURE_TAKEN;
import static com.example.atif.cameratrial.camera.CustomCamera.CameraState.STATE_PREVIEW;
import static com.example.atif.cameratrial.camera.CustomCamera.CameraState.STATE_WAITING_LOCK;
import static com.example.atif.cameratrial.camera.CustomCamera.CameraState.STATE_WAITING_NON_PRECAPTURE;
import static com.example.atif.cameratrial.camera.CustomCamera.CameraState.STATE_WAITING_PRECAPTURE;

/**
 * Created by atif on 21/01/17.
 * <h2>This class handles all the camera operation.</h2>
 * <p>This class can be externalized to access camera throughout the application</p>
 * <p>If camera is required only to get the image, then existing camera intent can be used to obtain the image</p>
 */
public class CustomCamera extends StateCallback implements SurfaceTextureListener, OnImageAvailableListener{


    private Context mContext;
    private CustomCameraInterface mCameraInterface;
    private final String TAG = this.getClass().getSimpleName();
    private final SparseArray<String> mCameraList = new SparseArray<>();

    enum CameraState{
        STATE_PREVIEW,
        STATE_WAITING_LOCK,
        STATE_WAITING_PRECAPTURE,
        STATE_WAITING_NON_PRECAPTURE,
        STATE_PICTURE_TAKEN
    }

    private final int MAX_PREVIEW_WIDTH = 1920;
    private final int MAX_PREVIEW_HEIGHT = 1080;

    private String mCameraId;
    private CustomCameraPreview mCustomCameraPreview;
    private CameraCaptureSession mCaptureSession;
    private CameraDevice mCameraDevice;
    private Size mPreviewSize;
    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private ImageReader mImageReader;
    private File mFile;
    private CaptureRequest.Builder mPreviewRequestBuilder;
    private CaptureRequest mPreviewRequest;
    private CameraState mState = STATE_PREVIEW;
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private boolean mFlashSupported;
    private CameraCharacteristics cameraCharacteristics;
    private int mSensorOrientation;
    private int currentCameraFacing;
    private Size currentImageCaptureSize;

    private CameraCaptureSession.CaptureCallback mCaptureCallback
            = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {
            switch (mState) {
                case STATE_PREVIEW: break;
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    if (afState == null) {
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            mState = STATE_PICTURE_TAKEN;
                            captureStillPicture();
                        } else {
                            runPrecaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE;
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        mState = STATE_PICTURE_TAKEN;
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };

    //Conversion from screen orientation to JPEG orientation
    private final SparseIntArray ORIENTATIONS = new SparseIntArray();

    /*Methods**************************************************************************************/

    //region Constructors
    public CustomCamera() throws IllegalArgumentException{
        throw new IllegalArgumentException("Please pass context as Parameter");
    }

    /**
     * <h4>Creates new object for the camera in activity that imoplement @link CustomCameraInterface</h4>
     * <p>This constructor by default opens the rear camera</p>
     * @param context context of the activity
     * @throws CameraException Throws CameraException if calling activity do not implement CustomCameraInterface
     */
    public CustomCamera(Context context) throws CameraException {
        this(context, CameraCharacteristics.LENS_FACING_BACK);
    }

    /**
     *<h4>Creates new object for the camera in activity that imoplement @link CustomCameraInterface</h4>
     *<p>This constructor can set the default facing of the camera</p>
     * @param context context of the activity
     * @param cameraFacing current facing of the camera, front or back.
     * @throws CameraException Throws CameraException if calling activity do not implement CustomCameraInterface
     */
    public CustomCamera(Context context, int cameraFacing) throws CameraException {
        if(context instanceof CustomCameraInterface){
            this.mContext = context;
            this.currentCameraFacing = cameraFacing;
            this.mCameraInterface = (CustomCameraInterface) context;
            initializeOrientations();
            setInitialParameters();
            setLargestAvailableSizeAsOutput(getCameraSupportedSize());
        }else{
            throw new CameraException("Your activity does not implement Custom Camera Interface");
        }
    }

    public CustomCamera(Context context, CustomCameraInterface mCameraInterface) {
        this(context, mCameraInterface, CameraCharacteristics.LENS_FACING_BACK);
    }

    public CustomCamera(Context context, CustomCameraInterface mCameraInterface, int cameraFacing){
        this.mContext = context;
        this.currentCameraFacing = cameraFacing;
        this.mCameraInterface = mCameraInterface;
        initializeOrientations();
        setInitialParameters();
        setLargestAvailableSizeAsOutput(getCameraSupportedSize());
    }
    //endregion

    //region Camera Controls

    public void setCameraPreview(CustomCameraPreview mCustomCameraPreview){
        this.mCustomCameraPreview = mCustomCameraPreview;
    }

    public void resumeCamera() throws CameraException{
        if(mCustomCameraPreview == null){
            throw new CameraException("camera preview surface not initialized");
        }
        startBackgroundThread();
        if (mCustomCameraPreview.isAvailable()) {
            openCamera(mCustomCameraPreview.getWidth(), mCustomCameraPreview.getHeight());
        } else {
            mCustomCameraPreview.setSurfaceTextureListener(this);
        }
    }

    public void pauseCamera(){
        closeCamera();
        stopBackgroundThread();
    }

    public void takePicture() {
        try {
            mFile = getStorageFile();
            lockFocus();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void switchCamera() throws CameraException{
        pauseCamera();
        currentCameraFacing = currentCameraFacing == CameraCharacteristics.LENS_FACING_BACK ? CameraCharacteristics.LENS_FACING_FRONT : CameraCharacteristics.LENS_FACING_BACK;
        resumeCamera();
    }

    public void setCurrentImageCaptureSize(Size currentImageCaptureSize) {
        Log.d(TAG, "Current size : "+currentImageCaptureSize);
        this.currentImageCaptureSize = currentImageCaptureSize;
        restartCameraWithChanges();
    }

    public void setZoom(){

    }

    /**
     * <h3>Set the focus region to tapped region</h3>
     * <p>Care must be taken to resize the focus region according to the sensor size</p>
     * <p>In general height is not considered as it is obtained from the sensor size and width of the focus region</p>
     * @see #getAppropriateMetringRectangle(int, int, int)
     * @see #cancelFocusRegion()
     * @param x left position of the focus region
     * @param y top position of the focus region
     * @param width width of the focusing region
     */
    public void setFocusRegion(int x, int y, int width){
        MeteringRectangle[] rectangle = new MeteringRectangle[]{
                getAppropriateMetringRectangle(x, y, width)
        };
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CaptureRequest.CONTROL_AF_TRIGGER_CANCEL);
        resetCameraPreview();
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_REGIONS, rectangle);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_REGIONS, rectangle);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CaptureRequest.CONTROL_AF_TRIGGER_START);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
        resetCameraPreview();
    }

    /**
     * <h3>Resets the focus to autofocus</h3>
     * <p>Call this method to reset the focus mode to auto picture from fixed focus</p>
     */
    public void cancelFocusRegion(){
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CaptureRequest.CONTROL_AF_TRIGGER_CANCEL);
        resetCameraPreview();
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
        resetCameraPreview();
    }

    public void setExposureTime(long exposureTime){
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_OFF);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_OFF);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_OFF);
        resetCameraPreview();
        mPreviewRequestBuilder.set(CaptureRequest.SENSOR_EXPOSURE_TIME, exposureTime);
        resetCameraPreview();
    }

    public void cancelExposureSettings(){
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AWB_MODE, CaptureRequest.CONTROL_AWB_MODE_AUTO);
        resetCameraPreview();
    }

    //endregion

    //region Get Current Status of Camera

    public int getCurrentCameraFacing(){
        return currentCameraFacing;
    }

    //endregion

    //region get Camera Details

    //initializes cameraCharacteristics if it is null
    private void setupCameraCharacteristics(){
        if(cameraCharacteristics == null){
            CameraManager manager = (CameraManager) mContext.getSystemService(Context.CAMERA_SERVICE);
            String cameraId = mCameraList.get(currentCameraFacing);
            try {
                cameraCharacteristics = manager.getCameraCharacteristics(cameraId);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public Range<Integer> getExposureLevels(){
        setupCameraCharacteristics();
        Range<Integer> exposureLevels = cameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE);
        if(exposureLevels == null){
            exposureLevels = new Range<>(100, 10000);
        }
        return exposureLevels;
    }

    public Range<Long> getExposureDuration(){
        setupCameraCharacteristics();
        Range<Long> exposureDuration = cameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE);
        if(exposureDuration == null){
            exposureDuration = new Range<>(100L, 10000L);
        }
        return exposureDuration;
    }

    public float getCameraMinFocalLength(){
        setupCameraCharacteristics();
        float minFocalLength = -1f;
        Float minFocalLengthBoxed = cameraCharacteristics.get(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE);
        if(minFocalLengthBoxed != null){
            minFocalLength = minFocalLengthBoxed;
        }
        return minFocalLength;
    }

    public int getMaxAfRegions(){
        setupCameraCharacteristics();
        int maxAfRegions = 0;
        Integer maxAfRegionsBoxed = cameraCharacteristics.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AF);
        if(maxAfRegionsBoxed != null){
            maxAfRegions = maxAfRegionsBoxed;
        }
        return maxAfRegions;
    }

    public int getMaxAeRegions(){
        setupCameraCharacteristics();
        int maxAeRegions = 0;
        Integer maxAeRegionsBoxed = cameraCharacteristics.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AE);
        if(maxAeRegionsBoxed != null){
            maxAeRegions = maxAeRegionsBoxed;
        }
        return maxAeRegions;
    }

    public int getMaxAwbRegions(){
        setupCameraCharacteristics();
        int maxAwbRegions = 0;
        Integer maxAwbRegionsBoxed = cameraCharacteristics.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AWB);
        if(maxAwbRegionsBoxed != null){
            maxAwbRegions = maxAwbRegionsBoxed;
        }
        return maxAwbRegions;
    }

    public List<Size> getCameraSupportedSize(){
        setupCameraCharacteristics();
        List<Size> supportedSize = new ArrayList<>();
        StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if(map != null) {
            supportedSize = Arrays.asList(map.getOutputSizes(ImageFormat.JPEG));
        }
        return supportedSize;
    }

    public Size[] getCameraOutputSizes(){
        setupCameraCharacteristics();
        Size[] outputSizes = new Size[0];
        StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if(map != null) {
            outputSizes = map.getOutputSizes(SurfaceTexture.class);
        }
        return outputSizes;
    }

    public Size getActivePixelArray(){
        setupCameraCharacteristics();
        return cameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE);
    }

    //endregion

    //region Helper Methods

    private void setInitialParameters() {
        try {
            mFile = getStorageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        CameraManager manager = (CameraManager) mContext.getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                Log.i(TAG, "Camera id is :"+cameraId);
                CameraCharacteristics characteristics
                        = manager.getCameraCharacteristics(cameraId);
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if(facing != null) {
                    mCameraList.append(facing, cameraId);
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private File getStorageFile() throws IOException{
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    private void initializeOrientations(){
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private void openCamera(int width, int height) {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            mCameraInterface.requestCameraPermission();
            return;
        }
        setUpCameraOutputs(width, height);
        configureTransform(width, height);
        CameraManager manager = (CameraManager) mContext.getSystemService(Context.CAMERA_SERVICE);
        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(mCameraId, this, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
    }

    /**
     * <h3>Closes the camera</h3>
     * @see #pauseCamera()
     * @see #stopBackgroundThread()
     */
    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * This method checks for the orientation of the screen and get the metering rectangle accordingly
     * @param x current touch position X
     * @param y current touch position Y
     * @param width width of the focus region
     * @return new Metering Rectangle based on screen orientation and sensor size
     */
    private MeteringRectangle getAppropriateMetringRectangle(int x, int y, int width){
        boolean isSwapNeeded = false;
        Log.d(TAG, "Camera Pixel "+getActivePixelArray()+" Current Size "+currentImageCaptureSize);
        int currentWidth = currentImageCaptureSize.getWidth();
        int currentHeight = currentImageCaptureSize.getHeight();
        int height = width*currentHeight/currentWidth;
        if(currentHeight < currentWidth){
            int temp = currentHeight;
            currentHeight = currentWidth;
            currentWidth  = temp;
            isSwapNeeded = true;
        }
        int previewWidth = mCustomCameraPreview.getWidth();
        float conversionFactor = (float)currentWidth/(float)previewWidth;
        int currentFocusX = Math.round(x*conversionFactor);
        int currentFocusY = Math.round(y*conversionFactor);
        if(isSwapNeeded){
            int temp = currentFocusY;
            currentFocusY = currentFocusX;
            currentFocusX = temp;
        }
        Log.d(TAG, "Current focus : "+String.valueOf(currentFocusX)+" "+String.valueOf(currentFocusY));
        return  new MeteringRectangle(currentFocusX, currentFocusY, width, height, MeteringRectangle.METERING_WEIGHT_MAX);
    }

    /**
     * <h3>Resets camera preview</h3>
     * <p>Call this method to reflect external while preview</p>
     */
    private void resetCameraPreview(){
        try {
            mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * <h3>Restarts the camera</h3>
     * <p>use this method to reflect any changes made externally</p>
     */
    private void restartCameraWithChanges(){
        pauseCamera();
        try {
            resumeCamera();
        } catch (CameraException e) {
            e.printStackTrace();
        }
    }

    //endregion

    //region Camera Setup Method

    private void configureTransform(int viewWidth, int viewHeight) {
        Activity activity = (Activity) mContext;
        if (null == mCustomCameraPreview || null == mPreviewSize || null == activity) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        mCustomCameraPreview.setTransform(matrix);
    }

    private void createCameraPreviewSession() {
        try {
            SurfaceTexture texture = mCustomCameraPreview.getSurfaceTexture();
            assert texture != null;

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder
                    = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.INFO_SUPPORTED_HARDWARE_LEVEL_FULL);
                                // Auto focus should be continuous for camera preview.
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                // Flash is automatically enabled when necessary.
                                setAutoFlash(mPreviewRequestBuilder);

                                // Finally, we start displaying the camera preview.
                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest,
                                        mCaptureCallback, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            mCameraInterface.notifyErrorOccurred("Unable to configure camera");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void lockFocus() {
        try {
            // This is how to tell the camera to lock focus.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the lock.
            mState = STATE_WAITING_LOCK;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void captureStillPicture() {
        try {
            final Activity activity = (Activity) mContext;
            if (null == activity || null == mCameraDevice) {
                return;
            }
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder =
                    mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(mImageReader.getSurface());

            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            setAutoFlash(captureBuilder);

            // Orientation
            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientation(rotation));

            CameraCaptureSession.CaptureCallback CaptureCallback
                    = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
                    Log.d(TAG, mFile.toString());
                    unlockFocus();
                }
            };

            mCaptureSession.stopRepeating();
            mCaptureSession.capture(captureBuilder.build(), CaptureCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            setAutoFlash(mPreviewRequestBuilder);
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
            // After this, the camera will go back to the normal state of preview.
            mState = STATE_PREVIEW;
            mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpCameraOutputs(int width, int height) {
        Activity activity = (Activity) mContext;
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraId = mCameraList.get(currentCameraFacing);
            cameraCharacteristics = manager.getCameraCharacteristics(cameraId);

            mImageReader = ImageReader.newInstance(currentImageCaptureSize.getWidth(), currentImageCaptureSize.getHeight(),
                    ImageFormat.JPEG, /*maxImages*/2);
            mImageReader.setOnImageAvailableListener(
                    this, mBackgroundHandler);

            // Find out if we need to swap dimension to get the preview size relative to sensor
            // coordinate.
            int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            //noinspection ConstantConditions
            mSensorOrientation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            boolean swappedDimensions = false;
            switch (displayRotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_180:
                    if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                        swappedDimensions = true;
                    }
                    break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_270:
                    if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                        swappedDimensions = true;
                    }
                    break;
                default:
                    Log.e(TAG, "Display rotation is invalid: " + displayRotation);
            }

            Point displaySize = new Point();
            activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
            int rotatedPreviewWidth = width;
            int rotatedPreviewHeight = height;
            int maxPreviewWidth = displaySize.x;
            int maxPreviewHeight = displaySize.y;

            if (swappedDimensions) {
                rotatedPreviewWidth = height;
                rotatedPreviewHeight = width;
                maxPreviewWidth = displaySize.y;
                maxPreviewHeight = displaySize.x;
            }

            if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                maxPreviewWidth = MAX_PREVIEW_WIDTH;
            }

            if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                maxPreviewHeight = MAX_PREVIEW_HEIGHT;
            }

            // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
            // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
            // garbage capture data.
            mPreviewSize = chooseOptimalSize(getCameraOutputSizes(),
                    rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                    maxPreviewHeight, currentImageCaptureSize);

            // We fit the aspect ratio of TextureView to the size of preview we picked.
            int orientation = mContext.getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mCustomCameraPreview.setAspectRatio(
                        mPreviewSize.getWidth(), mPreviewSize.getHeight());
            } else {
                mCustomCameraPreview.setAspectRatio(
                        mPreviewSize.getHeight(), mPreviewSize.getWidth());
            }

            // Check if the flash is supported.
            Boolean available = cameraCharacteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
            mFlashSupported = available == null ? false : available;

            mCameraId = cameraId;
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setLargestAvailableSizeAsOutput(List<Size> availableSizeAsPreview){
        /*largestSize = */currentImageCaptureSize = Collections.max(availableSizeAsPreview, new CompareSizesByArea());
        Log.d(TAG, "Current Image Capture Size "+currentImageCaptureSize);
    }

    private void setAutoFlash(CaptureRequest.Builder requestBuilder) {
        if (mFlashSupported) {
            requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                    CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
        }
    }

    private int getOrientation(int rotation) {
        // Sensor orientation is 90 for most devices, or 270 for some devices (eg. Nexus 5X)
        // We have to take that into account and rotate JPEG properly.
        // For devices with orientation of 90, we simply return our mapping from ORIENTATIONS.
        // For devices with orientation of 270, we need to rotate the JPEG 180 degrees.
        return (ORIENTATIONS.get(rotation) + mSensorOrientation + 270) % 360;
    }

    //endregion

    //region Preview Helpers

    private Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    //endregion

    //region Image Saver

    private class ImageSaver implements Runnable {

        /**
         * The JPEG image
         */
        private final Image mImage;
        /**
         * The file we save the image into.
         */
        private final File mFile;

        ImageSaver(Image image, File file) {
            mImage = image;
            mFile = file;
        }

        @Override
        public void run() {
            ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            try (FileOutputStream output = new FileOutputStream(mFile)){
                output.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mImage.close();
            }
            if(mFile.exists()){
                mCameraInterface.onImageProcessed(mFile.getAbsolutePath());
            }
        }

    }


    //endregion

    //region Background Threads

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //endregion

    //region Surface Texture Listener Methods

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        openCamera(width, height);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        configureTransform(width, height);
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    //endregion

    //region StateCallback methods

    @Override
    public void onOpened(@NonNull CameraDevice cameraDevice) {
        // This method is called when the camera is opened.  We start camera preview here.
        mCameraOpenCloseLock.release();
        mCameraDevice = cameraDevice;
        createCameraPreviewSession();
    }

    @Override
    public void onDisconnected(@NonNull CameraDevice cameraDevice) {
        mCameraOpenCloseLock.release();
        cameraDevice.close();
        mCameraDevice = null;
    }

    @Override
    public void onError(@NonNull CameraDevice cameraDevice, int error) {
        mCameraOpenCloseLock.release();
        cameraDevice.close();
        mCameraDevice = null;
        mCameraInterface.notifyErrorOccurred("Error at camera callback");
    }

    //endregion

    //region OnImageCallBack methods

    @Override
    public void onImageAvailable(ImageReader reader) {
        mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), mFile));
    }

    //endregion

    //region Comparator Class

    private class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    //endregion
}
