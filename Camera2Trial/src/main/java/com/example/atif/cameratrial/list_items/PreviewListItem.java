package com.example.atif.cameratrial.list_items;

import android.util.Size;

/**
 * Created by atif on 12/02/17.
 */

public class PreviewListItem {
    private Size currentSize;
    private float megaPixel;

    public PreviewListItem(Size currentSize, float megaPixel) {
        this.currentSize = currentSize;
        this.megaPixel = megaPixel;
    }

    public Size getCurrentSize() {
        return currentSize;
    }

    public float getMegaPixel() {
        return megaPixel;
    }
}
